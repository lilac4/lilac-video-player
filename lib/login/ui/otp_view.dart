import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lilac_video_player/login/controller/login_verify_controller.dart';
import 'package:lilac_video_player/utils/custom_text.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';


class OtpView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    if (Get.arguments!=null) {
      print("verificationId==" + Get.arguments[0]['verificationId']);
      LoginVerifyController.to.verificationId = Get.arguments[0]['verificationId'];
    }
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: GetBuilder<LoginVerifyController>(
          builder: (c) {
            return Column(
              children: [
                Spacer(),
                CustomText('Enter the OTP '),
                SizedBox(height: 35),
                OTPTextField(
                  length: 6,
                  width: MediaQuery.of(context).size.width,
                  fieldWidth: 45,
                  style: const TextStyle(
                      fontSize: 17
                  ),
                  textFieldAlignment: MainAxisAlignment.spaceAround,
                  fieldStyle: FieldStyle.box,
                  onChanged: (value) {

                  },
                  onCompleted: (pin) {
                    c.verifyOTP(verificationId: LoginVerifyController.to.verificationId, smsCode: pin);
                  },
                ),
                 Spacer(),
              ],
            );
          }
        ),
      ),
    );
  }
}
