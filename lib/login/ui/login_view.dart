import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lilac_video_player/login/controller/login_verify_controller.dart';
import 'package:lilac_video_player/utils/custom_text.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14.0),
          child: GetBuilder<LoginVerifyController>(
              builder: (c) {
                return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Spacer(),
                    CustomText('Phone Verification',
                      fontSize: 20,
                    ),
                    SizedBox(height: 10),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomText('Enter your phone number to receive OTP',
                          fontSize: 17,
                        ),
                      ],
                    ),
                    const SizedBox(height: 25),
                    TextFormField(
                          onChanged: (value) {
                            c.update();
                          },
                          controller: c.mobileController,
                      keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            prefix: CustomText(c.mobCode),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  color: Color(0xfff9c406))
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Color(0xfff9c406))
                            ),
                          ),
                        ),
                    Spacer(),
                    GestureDetector(
                      onTap: () {
                        c.verifyPhoneNumber();
                      },
                        child:
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: Center(
                            child: CustomText(
                              'Send the code', color: Colors.white,)),
                      )),
                    const SizedBox(height: 50),
              ]);
            }
          ),
        ),
      ),
    );
  }
}
