import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:lilac_video_player/login/ui/otp_view.dart';
import 'package:lilac_video_player/utils/snack_bar.dart';

import '../../video_activity/ui/video_player_view.dart';

class LoginVerifyController extends GetxController {
  static LoginVerifyController get to => Get.put(LoginVerifyController());

  String mobCode = '+91';
  String number = '';
  String verificationId = '';
  late TextEditingController mobileController;
  FirebaseAuth auth = FirebaseAuth.instance;
  final box = GetStorage();


  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    mobileController = TextEditingController();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    if (box.read('uid') != null||box.read('uid') != '') {
      Get.offAll(VideoPlayerView());
    }
  }

  void verifyPhoneNumber() async {
    if (mobileController.text.length == 10) {
    await FirebaseAuth.instance.verifyPhoneNumber(
    phoneNumber: mobCode+mobileController.text,
    verificationCompleted: (PhoneAuthCredential credential) {},
    verificationFailed: (FirebaseAuthException e) {
     snackBar(e.message.toString());
    },
    codeSent: (String verificationId, int? resendToken) {
      print('verificationId:> $verificationId');
     snackBar('An OTP has been send to your number..');
        Get.to(OtpView(),arguments: [{'verificationId': verificationId}]);
      },
      codeAutoRetrievalTimeout: (String verificationId) {  },
    );
  } else {
      snackBar('Phone number must be 10 digits');
    }
  }

  void verifyOTP({required String verificationId, required String smsCode}) async {
    var data;
    try {
      print('verificationId: $verificationId');
      print('smsCode: $smsCode');
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: verificationId, smsCode: smsCode);
      print('credential${credential.signInMethod}');
      //box.write('token', )
      // Sign the user in (or link) with the credential
      data = await auth.signInWithCredential(credential).then((value) {
        box.write('uid', data.user?.uid);
        Get.offAll(VideoPlayerView());
      });
      print('verified');
      print('verified${data.user?.uid}');
    } catch (e) {
      print('verifyOTPerror=$e');
    }
  }
}