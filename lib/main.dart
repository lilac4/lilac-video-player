import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:lilac_video_player/video_activity/controller/video_controller.dart';
import 'package:video_player/video_player.dart';

import 'login/controller/login_verify_controller.dart';
import 'login/ui/login_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true,
      ignoreSsl: true // option: set to false to disable working with http links (default: false)
  );
  await Firebase.initializeApp();
  await GetStorage.init();
  Get.put(LoginVerifyController());
  Get.put(VPlayerController());


  FlutterDownloader.registerCallback(DownloadStatusClass.callback);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginView(),
    );
  }
}

class DownloadStatusClass {
  static void callback(String id, DownloadTaskStatus status, int progress) {}
}
