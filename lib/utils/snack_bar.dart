
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import 'custom_text.dart';

snackBar(String message) {
  if (message != '' && Get.context != null) {
    Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      messageText: CustomText(message, fontSize: 12, color: Colors.white,),
      icon: Icon(
        Icons.info_outline_rounded,
        color: Colors.white,
      ),
      duration: Duration(seconds: 3),
    ).show(Get.context!);
  }}