import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screen_protector/screen_protector.dart';
import 'package:video_player/video_player.dart';
import 'package:path_provider/path_provider.dart';


class VPlayerController extends GetxController {

  static VPlayerController get to => Get.put(VPlayerController());

  late VideoPlayerController vController;
  int playingIndex = 0;
  ChewieController? chewieController;
  List url = ["https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4",
  "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4"];

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    ///to restrict screen capture
    await ScreenProtector.preventScreenshotOn();

    ///to start video at page open/ initialize video player
    vController = VideoPlayerController.network(url[playingIndex]);
    await vController.initialize();
    chewieController = ChewieController(
      videoPlayerController: vController,
      aspectRatio:16/9,
      autoInitialize: true,
      autoPlay: false,
      looping: false,
      materialProgressColors: ChewieProgressColors(
        playedColor: Color(0xff57EE9D),
        handleColor: Color(0xff57EE9D)
      ),
      showControls: true,
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
    update();
  }

  @override
  void dispose() {
    super.dispose();

    ///To close playing
    vController.dispose();
    chewieController?.dispose();
  }

  ///download file to store in device
  void downloadFile(String url) async {
    late PermissionStatus result;
    result = await Permission.notification.request();

    if (result.isGranted) {
      String filepath = (await getApplicationSupportDirectory()).path;
      FlutterDownloader.enqueue(
          saveInPublicStorage: true,
          url: url,
          savedDir: filepath,
          fileName: "documents_${DateTime.now()}.mp4",
          showNotification: true,
          openFileFromNotification: true);
    }
    update();
  }
}
