import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:lilac_video_player/login/ui/login_view.dart';
import 'package:video_player/video_player.dart';

import '../../utils/popup_menu.dart';
import '../controller/video_controller.dart';

class VideoPlayerView extends StatelessWidget {
  const VideoPlayerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VPlayerController>(
      init: VPlayerController(),
        builder: (c) {
      return SafeArea(
        child: Scaffold(
            backgroundColor: Color(0xffE5E5E5),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                c.chewieController != null
                    ? Stack(
                        children: [
                          AspectRatio(
                            aspectRatio: c.vController.value.aspectRatio,
                            child: Chewie(
                              controller: c.chewieController!,
                            ),
                          ),
                          PopupMenuButton<int>(
                                  offset: const Offset(0, -380),
                                  itemBuilder: (context) => [
                                    PopupMenuItem<int>(
                                            value: 0,
                                            child: GestureDetector(
                                              onTap: () {
                                                GetStorage().remove('uid');
                                                Get.offAll(LoginView());
                                              },
                                              child: PopUpMenuTile(
                                                isActive: true,
                                                title: 'Logout',
                                              ),
                                            )),
                                        PopupMenuItem<int>(
                                            value: 1,
                                            child: PopUpMenuTile(
                                              isActive: true,
                                              title: 'profile',
                                            )),
                                        PopupMenuItem<int>(
                                            value: 2,
                                            child: PopUpMenuTile(
                                              title: 'settings',
                                            )),
                                      ],child: Padding(
                                padding: EdgeInsets.only(left: 10, top: 5),
                                child: Image.asset(
                                  'assets/images/menuicon.png',
                                  fit: BoxFit.fill,
                                ),
                              ),),
                        ],
                      )
                    : Center(child: CircularProgressIndicator()),
                SizedBox(height: 5),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          c.vController =
                              VideoPlayerController.network(c.url[0]);
                          c.playingIndex = 0;
                          c.chewieController = ChewieController(
                            videoPlayerController: c.vController,
                            aspectRatio: 16 / 9,
                            autoInitialize: true,
                            autoPlay: false,
                            looping: false,
                            materialProgressColors: ChewieProgressColors(
                                playedColor: Color(0xff57EE9D),
                                handleColor: Color(0xff57EE9D)),
                            showControls: true,
                            errorBuilder: (context, errorMessage) {
                              return Center(
                                child: Text(
                                  errorMessage,
                                  style: TextStyle(color: Colors.white),
                                ),
                              );
                            },
                          );
                          c.update();
                        },
                        child: Container(
                          height: 43,
                          width: 43,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.arrow_back_ios_new_outlined,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          print('download');
                          c.downloadFile(c.url[c.playingIndex]);
                        },
                        child: Container(
                            height: 43,
                            width: 140,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.arrow_drop_down,
                                  size: 30,
                                  color: Color(0xff57EE9D),
                                ),
                                Center(
                                  child: Text(
                                    'Download',
                                  ),
                                ),
                              ],
                            )),
                      ),
                      GestureDetector(
                        onTap: () {
                          c.vController =
                              VideoPlayerController.network(c.url[1]);
                          c.playingIndex = 1;
                          c.chewieController = ChewieController(
                            videoPlayerController: c.vController,
                            aspectRatio: 16 / 9,
                            autoInitialize: true,
                            autoPlay: false,
                            looping: false,
                            materialProgressColors: ChewieProgressColors(
                                playedColor: Color(0xff57EE9D),
                                handleColor: Color(0xff57EE9D)),
                            showControls: true,
                            errorBuilder: (context, errorMessage) {
                              return Center(
                                child: Text(
                                  errorMessage,
                                  style: TextStyle(color: Colors.white),
                                ),
                              );
                            },
                          );
                          c.update();
                        },
                        child: Container(
                          height: 43,
                          width: 43,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.arrow_forward_ios_rounded,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )),
      );
    });
  }
}
